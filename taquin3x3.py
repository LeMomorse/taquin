import copy
import time
import random
import math

TAQUIN_SIZE = 3
TAQUIN_BUT = [[0,1,2],[3,4,5],[6,7,8]]

def colorization(color):
    if color == "red":
        return "\033[31m"
    elif color == "green":
        return "\033[32m"
    else:
        return "\033[0m"

def display_taquin(taquin, color = "red", start = ""):
    colorize = colorization(color)
    uncolorize = colorization("white")

    for i in range(TAQUIN_SIZE):
        print(f"{start}-------------")
        for j in range(TAQUIN_SIZE):
            if j == 0 :
                print(f"{start}| {colorize}{taquin[i][j] if taquin[i][j] != 0 else ' '}{uncolorize} | ", end="")
            elif j+1 == TAQUIN_SIZE:
                print(f"{colorize}{taquin[i][j] if taquin[i][j] != 0 else ' '}{uncolorize} |")
            else :
                print(f"{colorize}{taquin[i][j] if taquin[i][j] != 0 else ' '}{uncolorize} | ", end="")
    print(f"{start}-------------")


# the possibilities for the turn n°g
def new_possibilities(taquin, taquin_already_used, g):
    
    token_pos = pos_taquin(taquin, 0)
    neighbourhoods_pos = neighbourhoods_positions(taquin, token_pos)
    print(f"neighbourhoods_pos : {neighbourhoods_pos}")
    return moves(taquin, token_pos, neighbourhoods_pos, taquin_already_used, g)

# element position in the taquin / a position is a tuple like '(i,j)'
def pos_taquin(taquin, e):
    for i in range(TAQUIN_SIZE):
        for j in range(TAQUIN_SIZE):
            if(taquin[i][j] == e):
                return i,j
    raise Exception("Item not found")


# calculates the neighbourhoods positions
def neighbourhoods_positions(taquin, token_position):
    neighbourhoods_pos = []
    positions = [(token_position[0] - 1 , token_position[1]), (token_position[0] + 1 , token_position[1]), (token_position[0], token_position[1] - 1), (token_position[0], token_position[1] + 1)]
    for pos in positions:
        if(pos[0] >= 0 and pos[0] < TAQUIN_SIZE and pos[1] >= 0 and pos[1] < TAQUIN_SIZE):
            neighbourhoods_pos.append(pos)
    return neighbourhoods_pos


# adds a taquin in the priority log_file
def add_taquin(all_taquins_possibilities, taquindict : dict):    
    for i in range(len(all_taquins_possibilities)):
        if (all_taquins_possibilities[i]["heuristic"] + all_taquins_possibilities[i]["g"]) > (taquindict["heuristic"] + taquindict["g"]):
            all_taquins_possibilities.insert(i, taquindict)
            return
    all_taquins_possibilities.append(taquindict)


# calculates the possible moves to do for the turn
def moves(taquin, token_pos, neighbourhoods_pos, taquin_already_used, g):
    log_file.write(f"g : {g}\n")
    neighbourhoods_taquins = []

    for pos in neighbourhoods_pos:
        tmp_taquin = copy.deepcopy(taquin)  # immutable the list deeply
        
        tmp_permut = tmp_taquin[token_pos[0]][token_pos[1]]
        tmp_taquin[token_pos[0]][token_pos[1]] = tmp_taquin[pos[0]][pos[1]]
        tmp_taquin[pos[0]][pos[1]] = tmp_permut
        
        if(tmp_taquin not in taquin_already_used):
            taquin_dict = {"taquin" : tmp_taquin, "heuristic" : manhattan_taquin(tmp_taquin), "g" : g}
            add_taquin(neighbourhoods_taquins, taquin_dict)
            
            print(f"permutation : 0 with {taquin[pos[0]][pos[1]]} - {taquin_dict} => {taquin_dict['heuristic'] + taquin_dict['g']}")
            log_file.write(f"permutation : 0 with {taquin[pos[0]][pos[1]]} - {taquin_dict} => {taquin_dict['heuristic'] + taquin_dict['g']}\n")

    return neighbourhoods_taquins


# the best move to do for the turn
def best_move(all_taquins_possibilities):
    try :
        return all_taquins_possibilities[0]["taquin"]
    except (Exception):
        print("\nThere are no possibilities.")
        exit(1)


# calculates the manhattan distance for a token
def manhattan_token(taquin, token_position):
    final_pos = pos_taquin(TAQUIN_BUT, taquin[token_position[0]][token_position[1]]) # the pos in the TAQUIN_BUT
    return abs(token_position[0] - final_pos[0]) + abs(token_position[1] - final_pos[1])

# calculates the manhattan distance for a taquin
def manhattan_taquin(taquin):
    h = 0
    for i in range(TAQUIN_SIZE):
        for j in range(TAQUIN_SIZE):
            h += manhattan_token(taquin, (i,j))
    return h


# resolves a taquin
def resolve_taquin(taquin, debug_mode = False):
    taquin_already_used = []
    all_taquins_possibilities = []
    g = 0
        
    while(taquin != TAQUIN_BUT):
        
        print(f"g : {g}")
        display_taquin(taquin)
        taquin_already_used.append(taquin)
        
        possible_moves = new_possibilities(taquin, taquin_already_used, g)
        taquin = best_move(possible_moves)
        
        print("\n\n")
        g += 1
        
        if debug_mode:
            print(len(all_taquins_possibilities))
            time.sleep(1)
        
    taquin_already_used.append(taquin)
    
    print(f"g : {g}")
    display_taquin(taquin)

    print("\nTaquin resolved.")


# tests if the taquin is resolvable
def taquin_is_resolvable(taquin):
    flat_taquin = [e for l in taquin for e in l]
    nb_permut = 0
    for i in range(len(taquin)):
        for j in range(i, len(taquin)):
            if(flat_taquin[i] > flat_taquin[j] and flat_taquin[i] != 0 and flat_taquin[j] != 0):
                nb_permut += 1

    return nb_permut % 2 == 0



def main():
    my_taquin = [[0,1,6],[4,5,2],[7,3,8]]
    
    # -- Resolved --
    #my_taquin = [[3,1,2],[6,4,5],[0,7,8]]
    
    resolvable = taquin_is_resolvable(my_taquin)
    if resolvable:
        print(f"{colorization('green')}The taquin is resolvable!{colorization('white')}")

        debug_mode = input("\nYou want to use the debug mode (y,n) ? (time.sleep(1))\n") == 'y'
        
        t = time.time()
        resolve_taquin(my_taquin, debug_mode)
        print(f"{colorization('green')}Execution time : {round(time.time() - t, 2)}{colorization('white')}")
        
    else:
        print(f"{colorization('red')}The taquin is unresolvable...{colorization('white')}")
      

#open log file
global log_file
log_file = open("analyse_file.txt", "w") 
log_file.write(f"\n{time.time()}\n")
main()
log_file.close()